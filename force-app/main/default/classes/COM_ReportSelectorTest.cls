@IsTest
private class COM_ReportSelectorTest {

    @IsTest
    public static void testGetSObjectType(){
        System.assertEquals(Report.SObjectType, new COM_ReportSelector().getSObjectType());
    }

    @IsTest
    public static void selectByFolderDeveloperNamesTest(){
        COM_ReportSelector reportSelector = new COM_ReportSelector();
        reportSelector.selectByFolderDeveloperNames(new Set<String>{'SomeReport'});
    }

    @IsTest
    public static void selectByFolderDeveloperNameTest(){
        COM_ReportSelector reportSelector = new COM_ReportSelector();
        reportSelector.selectByFolderDeveloperName('SomeReport');
    }
}