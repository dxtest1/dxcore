/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group AutoNumber
* @group-content ../../ApexDocContent/AutoNumber.htm
*
* @description Providers the Selector for the implementation of the AutoNumber
* functionality.
*/
public virtual class COM_AutoNumberSelector extends fflib_SObjectSelector
{
    public virtual List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                COM_AutoNumber__c.Id,
                COM_AutoNumber__c.Auto_Number_Prefix__c,
                COM_AutoNumber__c.Counter__c,
                COM_AutoNumber__c.Entity__c,
                COM_AutoNumber__c.Field__c,
                COM_AutoNumber__c.Name
        };
    }

    public Schema.SObjectType getSObjectType()
    {
        return COM_AutoNumber__c.sObjectType;
    }

    public List<COM_AutoNumber__c> getAll() {
        return (List<COM_AutoNumber__c>) Database.query(newQueryFactory().toSOQL());
    }

    public COM_AutoNumber__c selectOneById(ID idOfRecord) {
        List<COM_AutoNumber__c> getRecord = this.selectById(new Set<Id> { idOfRecord });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }

    public List<COM_AutoNumber__c> selectById(Set<ID> idSet)
    {
        return (List<COM_AutoNumber__c>) selectSObjectsById(idSet);
    }

    public COM_AutoNumber__c selectOneByName(String name) {
        List<COM_AutoNumber__c> getRecord = this.selectByName(new Set<String> { name });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }

    public List<COM_AutoNumber__c> selectByName(Set<String> nameSet)
    {
        return (List<COM_AutoNumber__c>) Database.query(
                                                newQueryFactory().
                                                    setCondition(COM_AutoNumber__c.Name.getDescribe().getName() + ' IN :nameSet').
                                                    toSOQL()
                                                );
    }

}