/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Opportunity
* @group-content ../../ApexDocContent/Opportunity.htm
*
* @description A base Opportunity Selector. The class is a virtual class
* the virtual class means that it can be extended in applications. Then
* the base fields + base access patterns are accessible within that implementation.
*/
public virtual class COM_OpportunitySelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				Opportunity.Id,
				Opportunity.Amount,
				Opportunity.CloseDate,
				Opportunity.StageName,
				Opportunity.AccountId,
				Opportunity.OwnerId
		};
	}

	public Schema.SObjectType getSObjectType()
	{
		return Opportunity.sObjectType;
	}

    public List<Opportunity> getAll() {
        return (List<Opportunity>) Database.query(newQueryFactory().toSOQL());
    }

    public Opportunity selectOneById(ID idOfRecord) {
        List<Opportunity> getRecord = this.selectById(new Set<Id> { idOfRecord });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }


	public List<Opportunity> selectById(Set<ID> idSet)
	{
		return (List<Opportunity>) selectSObjectsById(idSet);
	}	

}