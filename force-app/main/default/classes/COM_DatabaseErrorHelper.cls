public with sharing class COM_DatabaseErrorHelper {

	public static Boolean isDuplicateError(Database.Error error) {
		return error instanceof Database.DuplicateError;
	}

	// example from https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_class_Datacloud_DuplicateResult.htm
	public static List<sObject> getDuplicatedRecord(Database.Error error) {
        // Get duplicate records
        List<sObject> duplicateRecords = new List<sObject>();

	    // If there are duplicates, an error occurs
	    // Process only duplicates and not other errors 
	    //   (e.g., validation errors)
	    if (isDuplicateError(error)) {
	        // Handle the duplicate error by first casting it as a 
	        //   DuplicateError class
	        // This lets you use methods of that class 
	        //  (e.g., getDuplicateResult())
	        Database.DuplicateError duplicateError = 
	                (Database.DuplicateError)error;
	        Datacloud.DuplicateResult duplicateResult = 
	                duplicateError.getDuplicateResult();

	        // Return only match results of matching rules that 
	        //  find duplicate records
	        Datacloud.MatchResult[] matchResults = 
	                duplicateResult.getMatchResults();

	        // Just grab first match result (which contains the 
	        //   duplicate record found and other match info)
	        Datacloud.MatchResult matchResult = matchResults[0];

	        Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

	        // Add matched record to the duplicate records variable
	        for (Datacloud.MatchRecord matchRecord : matchRecords) {
	            //System.debug('MatchRecord: ' + matchRecord.getRecord());
	            duplicateRecords.add(matchRecord.getRecord());
	        }
	    }
	    return duplicateRecords;
	}
}