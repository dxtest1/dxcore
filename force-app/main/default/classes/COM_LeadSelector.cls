public virtual class COM_LeadSelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				Lead.Id,
				Lead.Name,
				Lead.LastName,
				Lead.Street,
				Lead.City,
				Lead.CountryCode,
				Lead.StateCode,
				Lead.PostalCode,
				Lead.Address
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return Lead.sObjectType;
	}

	public List<Lead> selectById(Set<ID> idSet)
	{
		return (List<Lead>) selectSObjectsById(idSet);
	}	

    public Lead selectOneById(ID idOfRecord) {
        List<Lead> get_lead = this.selectById(new Set<Id> { idOfRecord });
        return get_lead.size() > 0 ? get_lead[0] : null;
    }


    public Lead selectOneByName(String name) {
        List<Lead> get_lead = this.selectByName(new Set<String> { name });
        return get_lead.size() > 0 ? get_lead[0] : null;
    }

    public List<Lead> selectByName(Set<String> nameSet)
    {
        return (List<Lead>) Database.query(
                                                newQueryFactory().
                                                    setCondition(Lead.Name.getDescribe().getName() + ' IN :nameSet').
                                                    toSOQL()
                                                );
    }

    public List<Lead> selectByFilter(String whereFilter)
    {
        return (List<Lead>)  Database.query(newQueryFactory().
                                            setCondition(whereFilter).
                                            toSOQL()
                                        );
    }

    public Database.QueryLocator queryLocatorByFilter(String whereFilter)
    {
        return Database.getQueryLocator(newQueryFactory().
                                            setCondition(whereFilter).
                                            toSOQL()
                                        );
    }
}