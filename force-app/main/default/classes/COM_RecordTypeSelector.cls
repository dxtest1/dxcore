public virtual class COM_RecordTypeSelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				RecordType.Id,
				RecordType.Name, 
				RecordType.Description,
				RecordType.DeveloperName
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return null; 
	}

    public RecordType selectOneById(ID idOfRecord) {
        List<RecordType> getRecord = this.selectById(new Set<Id> { idOfRecord });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }

	public List<RecordType> selectById(Set<ID> idSet)
	{
		return [SELECT
					Id,
					Name,
					Description,
					DeveloperName
			   FROM RecordType
			   WHERE Id IN :idSet];
	}
	
	public Schema.RecordTypeInfo selectDefaultBySObjectType(String sObjectName) {
		Schema.DescribeSObjectResult dsr = Schema.describeSObjects(new List<String>{ sObjectName })[0];
		Schema.RecordTypeInfo defaultRecordType;
		for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
			if(rti.isDefaultRecordTypeMapping()) {
				defaultRecordType = rti;

			}
		}
		return defaultRecordType;
	} 


	public List<RecordType> selectAvailableBySobjectType(
			Schema.SObjectType sObjectT
	) {
		return selectBySobjectType(sObjectT, true, false);
	}

	public List<RecordType> selectAvailableBySobjectName(
			String sObjectName
	) {
		Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sObjectName);

		return selectBySobjectType(convertType, true, false);
	}

	public List<RecordType> selectBySobjectType(
			Schema.SObjectType sObjectT,
			Boolean enforceAccess,
			Boolean showInActive
	)
	{
		Set<Id> availableRecordTypes = new Set<Id>();
		for( Schema.RecordTypeInfo recordType : sObjectT.getDescribe().getRecordTypeInfos()){
			if((recordType.IsActive() || showInActive)) {
				if((enforceAccess && recordType.isAvailable()) || (!enforceAccess)) {
					availableRecordTypes.add(recordType.getRecordTypeId());
				}
		  	}
		}
		List<RecordType> recordTypesList = this.selectById(availableRecordTypes);

		return recordTypesList;
	}
	
	public Map<String,RecordType> getAllRecordTypesBySobjectType(Schema.SObjectType sObjectT, Boolean enforceAccess, Boolean showInActive)
	{
		Map<String,RecordType> map_recordType = new Map<String,RecordType>();
		COM_RecordTypeSelector rts = new COM_RecordTypeSelector();
		for(RecordType rt : rts.selectBySobjectType(sObjectT, enforceAccess, showInActive)) {
			map_recordType.put(rt.DeveloperName, rt);
		}
		return map_recordType;
	}
}