@IsTest
private class COM_ReportListCmpControllerTest {

    @IsTest
    public static void testSelectReportsByDeveloperName(){
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        COM_ReportSelector reportSelector = (COM_ReportSelector) mocks.mock(COM_ReportSelector.class);

        List<Report> reportList = (List<Report>) JSON.deserialize('[{"Name": "Name1", "DeveloperName": "Name1", "Id": "' + fflib_IDGenerator.generate(Report.SObjectType) + '"}, ' +
                '{"Name": "Name2", "DeveloperName": "Name2", "Id": "' + fflib_IDGenerator.generate(Report.SObjectType) + '"}]', List<Report>.class);
        mocks.startStubbing();
        mocks.when(reportSelector.selectByFolderDeveloperName('ReportFolder')).thenReturn(reportList);
        mocks.stopStubbing();

        COM_ReportListCmpController.selector = reportSelector;

        List<COM_ReportListCmpController.ReportWrapper> reportWrappers = COM_ReportListCmpController.selectReportsByFolderDeveloperName('ReportFolder');

        ((COM_ReportSelector) mocks.verify(reportSelector)).selectByFolderDeveloperName('ReportFolder');

        System.assertEquals(2, reportWrappers.size());

        Map<String, Id> reportNameToIdMap = new Map<String, Id>();
        for(Report r : reportList){
            reportNameToIdMap.put(r.DeveloperName, r.Id);
        }
        for(COM_ReportListCmpController.ReportWrapper wrapper : reportWrappers){
            System.assert(wrapper.reportLabel == wrapper.reportName);
            System.assertEquals('/' + reportNameToIdMap.get(wrapper.reportName), wrapper.reportUrl);
        }
    }
}