@isTest
public with sharing class COM_HelperTest {


    @testSetup static void testSetup() {
        List<COM_OptionsModel> optionsModelList = new List<COM_OptionsModel> ();
        optionsModelList.add(new COM_OptionsModel('Patient Qualification', 'Patient Qualification'));
        optionsModelList.add(new COM_OptionsModel('Test1', 'test'));

        insert new PicklistValues__c(ObjNameFieldRecordTypeIdKey__c='Opportunity-StageName-0127E000000dfGiQAI', values__c= JSON.serialize(optionsModelList));
    }

    /*
    * Check if method COM_Helper.getPicklistForOptionsModels will execute without returning any values with empty parameters.
    */
    @isTest
    public static void test_getPicklistForOptionsModels_emptyParameters() {

        Test.startTest();
        List<COM_OptionsModel> picklistValuesList = COM_Helper.getPicklistForOptionsModels('','','');
        Test.stopTest();

        System.assertEquals(0, picklistValuesList.size());
    }

    /*
    * Check if method COM_Helper.getPicklistForOptionsModels will execute without returning any values with wrong parameters.
    */
    @isTest
    public static void test_getPicklistForOptionsModels_wrongParameter() {

        Test.startTest();
        List<COM_OptionsModel> picklistValuesList = COM_Helper.getPicklistForOptionsModels('Account','StageName','0127E000000dfGiQAI');
        Test.stopTest();

        System.assertEquals(0, picklistValuesList.size());
    }

    /*
    * Check if method COM_Helper.getPicklistForOptionsModels will execute and return lsit of the COM_OptionsModel.
    */
    @isTest
    public static void test_getPicklistForOptionsModels_ok() {

        Test.startTest();
        List<COM_OptionsModel> picklistValuesList = COM_Helper.getPicklistForOptionsModels('Opportunity','StageName','0127E000000dfGiQAI');
        Test.stopTest();

        System.assertEquals(2, picklistValuesList.size());
    }
}
