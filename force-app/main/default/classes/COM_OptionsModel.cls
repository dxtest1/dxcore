public with sharing class COM_OptionsModel {
    @AuraEnabled
    public String label { get; set;}
    @AuraEnabled
    public String value { get; set;}
    @AuraEnabled
    public String selected { get; set;}
    
    public COM_OptionsModel(String label, String value) {
        this.label = label;
        this.value = value;
    }

}