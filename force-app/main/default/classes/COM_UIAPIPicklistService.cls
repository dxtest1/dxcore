/**
* @author David Fannar Gunnarsson
* @date 2019-10-16
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description a common service class mainly used by the schedule job COM_UIAPIPicklistScheduleJob to populate/update/delete the PicklistValues__c records base on the UIAPIPicklistsToFetch__mdt add the UI API calls.
*/
public with sharing class COM_UIAPIPicklistService {

    private static String COM_UIAPI_Endpoint = '/services/data/v46.0/ui-api/object-info/{0}/picklist-values/{1}/{2}';


    /**
    * Method to get all picklist values from UI API service by http callouts for given sObjectName, fieldName and RedordTypeId
    */
    public static List<COM_OptionsModel> getValues(String objectType, String fieldName, String recordTypeId) {
        //Endpoint
        String endpoint = URL.getSalesforceBaseUrl().toExternalForm() + COM_UIAPI_Endpoint;
        endpoint = String.format(endpoint, new String[]{ objectType, recordTypeId, fieldName });
        EncodingUtil.urlEncode(endpoint,'UTF-8');
        
        //HTTP Request send
        HTTPResponse res = new HttpCalloutService(endpoint).addHeaderLine('Authorization', 'OAuth ' + System.UserInfo.getSessionId()).get();
        
        //Parse response
        COM_UIAPIModel model = COM_UIAPIModel.parse(res.getBody());
        return model.values;
    }

    /**
    * Method to get all picklist values from UI API service by http callouts for given sObjectNames, fieldNames defined in the UIAPIPicklistsToFetch__mdt.
    * Method will also get all avaible reord types from Schema.
    * Method will create and return a map from  "sObjectName + '-' + fieldName + '-' + recordTypeId" to the serialized picklist values.
    */
    public static Map<String, String> getNewPicklistValuesFromUIAPIMap() {
        Map<String, String> sObjectFieldTypeIdToPicklistValuesMap = new  Map<String, String>();

        List<UIAPIPicklistsToFetch__mdt> picklistsToFetchList = new COM_UIAPIPicklistsToFetchSelector().selectAll(); 
        for(UIAPIPicklistsToFetch__mdt picklistsToFetch : picklistsToFetchList) {
            String objectStr = picklistsToFetch.sObjectName__c;
            String fieldNameStr = picklistsToFetch.fieldName__c;

            Schema.SObjectType convertSObjectType = Schema.getGlobalDescribe().get(objectStr);
            Map<String, Schema.RecordTypeInfo> recordTypes = convertSObjectType.getDescribe().getRecordTypeInfosByName();
            for(Schema.RecordTypeInfo recordType : recordTypes.values()) {
                List<COM_OptionsModel> picklistValues = getValues(objectStr, fieldNameStr, recordType.getRecordTypeId());
                sObjectFieldTypeIdToPicklistValuesMap.put(
                    getMapKey(objectStr, fieldNameStr, recordType.getRecordTypeId()), 
                    JSON.serialize(picklistValues)
                );
            }
        }

        return sObjectFieldTypeIdToPicklistValuesMap;
    }

    /**
    * Method to update/insert/delete PicklistValues__c base on the newPicklistValuesMap. Method will commit changes into database.
    */
    public static void commitNewPicklistValues(Map<String, String> newPicklistValuesMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> { PicklistValues__c.SObjectType});

        List<PicklistValues__c> actualPicklistValuesList = new COM_PicklistValuesSelector().getAll();
        for(PicklistValues__c actualPicklistValue : actualPicklistValuesList) {
               if(newPicklistValuesMap.containsKey(actualPicklistValue.ObjNameFieldRecordTypeIdKey__c)) {
                     String newPicklisstValuesString =  newPicklistValuesMap.remove(actualPicklistValue.ObjNameFieldRecordTypeIdKey__c);
                     actualPicklistValue.values__c = newPicklisstValuesString;
                     uow.registerDirty(actualPicklistValue);
               } else {
                     uow.registerDeleted(actualPicklistValue);
               }
        }

        for(String picklistValueToInsert : new List<String>(newPicklistValuesMap.keySet()) ) {
               uow.registerNew( new PicklistValues__c(ObjNameFieldRecordTypeIdKey__c=picklistValueToInsert, values__c=newPicklistValuesMap.get(picklistValueToInsert) ) );
        }
        
         uow.commitWork();
    }

    /**
    * Method to create a key "sObjectName + '-' + fieldName + '-' + recordTypeId" to easily procede picklist values.
    */
    public static String getMapKey(String sObjectName, String fieldName, String recordTypeId) {
            return sObjectName + '-' + fieldName + '-' + recordTypeId;
    }
}
