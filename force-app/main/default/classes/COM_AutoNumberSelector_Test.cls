/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group AutoNumber
* @group-content ../../ApexDocContent/AutoNumber.htm
*
* @description The Test class for testing the AutoNumberSelector. The 
* AutoNumberSelector is a Selector class that helps with generation autoNumbers in
* a code driven way, where there is a need to delegate a number to the system
*/
@isTest()
public with sharing class COM_AutoNumberSelector_Test {

    @testSetup 
    static void testSetup() {
        COM_AutoNumberTestFactory helper = new COM_AutoNumberTestFactory(
                                                                            'OpportunityNumber', 
                                                                            'Opportunity',
                                                                            'OpportunityNumber__c'
                                                                        ).save();

    }
    
    @isTest()
    static void test_getAutoNumberFromSetup() {
        COM_AutoNumberSelector autonumberSelector = new COM_AutoNumberSelector();
        List<COM_AutoNumber__c> autonumberRecord = autonumberSelector.getAll();
        
        COM_AutoNumber__c autonumberRecordById = autonumberSelector.selectOneById(autonumberRecord[0].Id);
        COM_AutoNumber__c autonumberRecordByName = autonumberSelector.selectOneByName(autonumberRecord[0].Name);

        System.assertEquals('OpportunityNumber', autonumberRecord[0].Name);
        System.assertEquals('OpportunityNumber', autonumberRecordById.Name);
        System.assertEquals('OpportunityNumber', autonumberRecordByName.Name);
    }



}
