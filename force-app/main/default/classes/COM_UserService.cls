/**
* @author David Fannar Gunnarsson
* @date 2019-10-16
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description a common User Service class for using in other packages
*/
public virtual class COM_UserService {
    public static Id addPermissionSetToUser(String username, String permissionSet) { 

        COM_UserSelector user_selector = new COM_UserSelector();
        COM_PermissionSetSelector perm_selector = new COM_PermissionSetSelector();
        User userSelected = user_selector.selectOneByUsername(username);
        PermissionSet permissionSetSelected = perm_selector.selectOneByName(permissionSet);

        PermissionSetAssignment psa = new PermissionSetAssignment(
                                            PermissionSetId = permissionSetSelected.Id, 
                                            AssigneeId = userSelected.Id);
        insert psa;      

        return psa.Id;
    }
}
