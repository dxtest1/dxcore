public interface COM_IAccountSelector extends fflib_ISObjectSelector 
{
    List<Account> getAll();
	Account selectOneById(ID id);
	List<Account> selectById(Set<ID> idSet);
	Account selectOneByName(String name);
	List<Account> selectByName(Set<String> nameSet);
    List<Account> selectByOpportunity(List<Opportunity> opportunities);
}