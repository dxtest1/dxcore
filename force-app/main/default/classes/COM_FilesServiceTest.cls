@isTest
public with sharing class COM_FilesServiceTest {

    @isTest
    public static void test_saveFileAsContentVersion() {
        string filename = 'test.pdf';
        String fileTitle = 'test';
        Blob bobData = Blob.toPdf('test content');
        Id linkedEntityId = UserInfo.getUserId();

        Account acc = new COM_AccountTestFactory().save().getRecord();

        Test.startTest();
            COM_FilesService.saveFileAsContentVersion(filename, fileTitle, bobData, acc.Id);
        Test.stopTest();

        List<ContentVersion> conVersionList = [Select id, VersionData from ContentVersion];
        System.assertEquals(1, conVersionList.size());
    }

    //Only one ContentVersion document should be added after adding multiple fles with the same name.
    @isTest
    public static void test_saveFileAsContentVersion_multipleUpdateFile() {
        string filename = 'test.pdf';
        String fileTitle = 'test';
        Blob bobData = Blob.toPdf('test content');
        Blob finalBlobData = Blob.toPdf('test content2222');
        Id linkedEntityId = UserInfo.getUserId();

        Account acc = new COM_AccountTestFactory().save().getRecord();

        Test.startTest();
            COM_FilesService.saveFileAsContentVersion(filename, fileTitle, bobData, acc.Id);
            COM_FilesService.saveFileAsContentVersion(filename, fileTitle, finalBlobData, acc.Id);
        Test.stopTest();

        List<ContentVersion> conVersionList = [Select id, VersionData from ContentVersion];
        System.assertEquals(1, conVersionList.size());
    }

}
