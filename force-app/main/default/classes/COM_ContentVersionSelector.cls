/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Content
* @group-content ../../ApexDocContent/Content.htm
*
* @description Selector class for ContentVersion, which offers access patterns for the
* ContentVersion. All Access patterns to this objects should be defined here and direct
* SOQL queries in Service class should be avoided.
*/
public with sharing class COM_ContentVersionSelector  extends fflib_SObjectSelector {
	public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
				ContentVersion.Id,
                ContentVersion.ContentDocumentId
			};
	}

	public Schema.SObjectType getSObjectType() {
		return ContentVersion.sObjectType;
	}

	public List<ContentVersion> selectById(Set<Id> idSet) {
		return (List<ContentVersion>) selectSObjectsById(idSet);
	}	

    public ContentVersion selectOneById(Id idOfRecord) {
        List<ContentVersion> getRecord = this.selectById(new Set<Id> { idOfRecord });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }

    public List<ContentVersion> selectByFileName(String filename) {
        return (List<ContentVersion>) Database.query(
                                                newQueryFactory().
                                                    setCondition(ContentVersion.PathOnClient + ' = :filename AND ' + ContentVersion.isMajorVersion + ' = false').
                                                    toSOQL()
                                                );
    }
}
