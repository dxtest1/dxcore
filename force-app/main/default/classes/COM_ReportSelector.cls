public virtual class COM_ReportSelector extends fflib_SObjectSelector{

    public SObjectType getSObjectType() {
        return Report.sObjectType;
    }

    public List<SObjectField> getSObjectFieldList() {
        return new List<SObjectField>{
                Report.Id,
                Report.Name,
                Report.DeveloperName
        };
    }

    public List<Report> selectByFolderDeveloperNames(Set<String> folderDeveloperNames) {
        return (List<Report>) Database.query(
                newQueryFactory().
                        setCondition('OwnerId IN (SELECT Id FROM Folder WHERE DeveloperName = :folderDeveloperNames)').
                        setOrdering(new fflib_QueryFactory.Ordering(Report.Name, fflib_QueryFactory.SortOrder.ASCENDING)).
                        toSOQL()
        );
    }

    public List<Report> selectByFolderDeveloperName(String folderDeveloperName) {
        return selectByFolderDeveloperNames(new Set<String>{folderDeveloperName});
    }

}