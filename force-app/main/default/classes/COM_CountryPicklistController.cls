/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Common
* @group-content ../../ApexDocContent/Common.htm
*
* @description For Apex code it's not possible to get the country & state picklist with standard commands when
* using country & state picklist. This common library is a controller for the lightning component that displays
* this relationship.
*/

public with sharing class COM_CountryPicklistController {

    /*******************************************************************************************************
    * @description Method for getting all of the country and states in a country based on the Salesforce metadata
    * @return JSON serialized string of the map between country and states for using in the lightning components
    * puts less work on network latency but increases the evaluation of user.
    */
    @AuraEnabled
    public static String getCountryStateMap() {
        String mapCS;
        List<Schema.PicklistEntry> ctrl_ple =  User.Countrycode.getDescribe().getPickListValues();
        List<Schema.PicklistEntry> dep_ple = User.Statecode.getDescribe().getPickListValues();

        mapCS = JSON.serialize( COM_DependentPicklistHelper.getDependentPicklist(ctrl_ple, dep_ple));
        return mapCS;
    }

}