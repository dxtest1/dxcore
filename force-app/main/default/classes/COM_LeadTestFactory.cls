@isTest()
public class COM_LeadTestFactory {
        private Lead obj;

        public COM_LeadTestFactory() {
            obj = new Lead( LastName = 'testName',
                            Email = 'test123@test.com',
                            Company = 'test comapny.',
                            CountryCode = 'DE'
            );
        }

        public COM_LeadTestFactory withFirstName(String firstName) {
            obj.FirstName = firstName;
            return this;
        }

        public COM_LeadTestFactory withLastName(String lastName) {
            obj.LastName = lastName;
            return this;
        }


        public COM_LeadTestFactory   save() {
            insert obj;
            return this;
        }

        public Lead getRecord() {
            return obj;
        }
}