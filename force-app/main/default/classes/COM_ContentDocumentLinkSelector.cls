/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Content
* @group-content ../../ApexDocContent/Content.htm
*
* @description Selector class for ContentDocumentLink, which offers access patterns for the
* ContentDocumentLink. All Access patterns to this objects should be defined here and direct
* SOQL queries in Service class should be avoided.
*/
public virtual class COM_ContentDocumentLinkSelector  extends fflib_SObjectSelector {
	public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
				ContentDocumentLink.Id,
                ContentDocumentLink.ContentDocumentId
			};
	}

	public Schema.SObjectType getSObjectType() {
		return ContentDocumentLink.sObjectType;
	}

	public List<ContentDocumentLink> selectById(Set<Id> idSet) {
		return (List<ContentDocumentLink>) selectSObjectsById(idSet);
	}	

    public ContentDocumentLink selectOneById(Id idOfRecord) {
        List<ContentDocumentLink> getRecord = this.selectById(new Set<Id> { idOfRecord });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }

	public List<ContentDocumentLink> selectByLinkedEntityId(Id linkedEntityId) {
		return (List<ContentDocumentLink>) Database.query('select id, ContentDocumentId, ContentDocument.Title from ContentDocumentLink where LinkedEntityId = :linkedEntityId');
	}
}