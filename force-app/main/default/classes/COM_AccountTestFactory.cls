/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Account
* @group-content ../../ApexDocContent/Account.htm
*
* @description COM_AccountTestFactory offers methods for other applications 
* to be able to manipulate the Account record easily. It's not extendable (virtual)
* since Test classes can't extend other Tests class ( at least as of Nov 2019 )
*/
@isTest()
public class COM_AccountTestFactory {

        private Account obj;
        public COM_AccountTestFactory(String Name,
                String ShippingStreet,
                String ShippingCountryCode,
                String ShippingPostalCode,
                String ShippingStateCode,
                String ShippingCity,
                String BillingStreet,
                String BillingCountryCode,
                String BillingPostalCode,
                String BillingStateCode,
                String BillingCity      
            ) {
            obj = new Account(
                    Name = Name,
                    ShippingStreet = ShippingStreet,
                    ShippingCountryCode= ShippingCountryCode,
                    ShippingPostalCode = ShippingPostalCode,
                    ShippingStateCode=ShippingStateCode,
                    ShippingCity=ShippingCity,
                    BillingStreet = ShippingStreet,
                    BillingCountryCode= ShippingCountryCode,
                    BillingPostalCode = ShippingPostalCode,
                    BillingStateCode=ShippingStateCode,
                    BillingCity=ShippingCity
            );
        }

        public COM_AccountTestFactory() {
            this(
                    'Test' + String.valueOf(Math.random()),
                    'TEST Test',
                    'DE',
                    '81100',
                    '',
                    'Munchen',
                    'Test Test',
                    'DE',
                    '81100',
                    '',
                    'Munchen'
            );
        }

        public COM_AccountTestFactory withName(String Name) {
            obj.Name = Name;
            return this;
        }


        public COM_AccountTestFactory withBillingCountryCode(String billingCountryCode) {
            obj.BillingCountryCode = billingCountryCode;
            return this;
        }

        public COM_AccountTestFactory withBillingCountry(String billingCountry) {
            obj.BillingCountry = billingCountry;
            return this;
        }

        public COM_AccountTestFactory withBillingPostalCode(String billingPostalCode) {
            obj.BillingPostalCode = billingPostalCode;
            return this;
        }

        public COM_AccountTestFactory save() {
            insert obj;
            return this;
        }

        public Account getRecord() {
            return obj;
        }
}