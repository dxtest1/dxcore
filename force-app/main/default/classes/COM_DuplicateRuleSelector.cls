public with sharing class COM_DuplicateRuleSelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				DuplicateRule.Id,
				DuplicateRule.DeveloperName
			};
	}

	public Schema.SObjectType getSObjectType() {
		return null; 
	}

	public DuplicateRule selectOneByDeveloperName(String name) {
		List<DuplicateRule> getRecord = this.selectByDeveloperName(new Set<String>{
				name
		});
        return getRecord.size() > 0 ? getRecord[0] : null;
	}

	public List<DuplicateRule> selectByDeveloperName(Set<String> names) {
        return [SELECT
					Id,
					DeveloperName
			   FROM DuplicateRule
			   WHERE DeveloperName LIKE :names];
	}

}