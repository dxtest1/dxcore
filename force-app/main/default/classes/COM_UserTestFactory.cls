public class COM_UserTestFactory {
	private User obj;

	// ProfileName is required even for Chatter Free users
	// TODO: Add a method and Selector for Roles
	public COM_UserTestFactory(String profileName) {
			COM_ProfileSelector profileSelector = new COM_ProfileSelector();
			obj = new User(
				alias = 'TestUser', 
				email='test@example.com',
				emailencodingkey='UTF-8', 
				lastname='Testing', 
				languagelocalekey='en_US', 
				localesidkey='en_US', 
				profileid = profileSelector.selectOneByName(profileName).Id,
				timezonesidkey='Europe/Berlin', 
				username= System.now().millisecond() + 'test@example.com'
			);
	}

	public COM_UserTestFactory save() {
		COM_UserSelector user_selector = new COM_UserSelector();
		if(user_selector.selectOneByUsername(this.obj.username) != null) {
			String random_ness = String.valueOf(Math.random());
			this.obj.username = this.obj.username + random_ness;
			this.obj.alias = ('Test' + random_ness);
		}
		insert obj;
		return this;
	}

	public User getRecord() {
		return obj;
	}
}