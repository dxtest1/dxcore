/**
* @author David Fannar Gunnarsson
* @date 05-11-2020
*
* @group Chatter
* @group-content ../../ApexDocContent/Chatter.htm
*
* @description Service Class that allows for sending Chatter messages with a mention
* people or other groups. This is not possible from Apex / Flows / Process Builders
* You can @-mention a user or group by using
*                         the syntax {ID}, for example: 'Hello {005x0000000URNP}, have you
*                         seen the group {0F9x00000000D7m}?' 
*/
public class COM_ChatterPublisherService {
    private String networkId;

    /*******************************************************************************************************
    * @description Constructor for the Service class
    * @param networkId the networkId of the network being used, this can be left as null if you are not posting to a community
    * @return instance of the class
    * @example
    * COM_ChatterPublisherService chatterPublisherService = new COM_ChatterPublisherService('123123');
    */
    public COM_ChatterPublisherService(String networkId) {
        this.networkId = networkId == null || networkId.length() == 0 ? Network.getNetworkId() : networkId;
    }

    /*******************************************************************************************************
    * @description Constructor for the Service class, without specificing the network, this select the current
    * network Id of the executed context
    * @return instance of the class
    * @example
    * COM_ChatterPublisherService chatterPublisherService = new COM_ChatterPublisherService();
    */
    public COM_ChatterPublisherService() {
        this(
            null
        );
    }

    /*******************************************************************************************************
    * @description Method for sending Chatter Messages 
    * @param itemToPostTo The parent of the post. Can be a user ID, a group ID, or a record ID.
    * @param message The text of the post. You can @-mention a user or group by using
    *                         the syntax {ID}, for example: 'Hello {005x0000000URNP}, have you
    *                         seen the group {0F9x00000000D7m}?' Links and hashtags will be
    *                         automatically parsed if provided.
    * @return Id Id of the FeedElement that has been created
    * @example
    * COM_ChatterPublisherService chatterPublisherService = new COM_ChatterPublisherService();
    * chatterPublisherService.sendMessage('005x0000000URNP', 'Hello {005x0000000URNP}, have you
    *                         seen the group {0F9x00000000D7m}?');
    */
    public Id sendMessage(String itemToPostTo, String message) {
        ConnectApi.FeedItem feedItem = (ConnectApi.FeedItem)ConnectApiHelper.postFeedItemWithMentions(
                                                                                this.networkId,
                                                                                itemToPostTo,
                                                                                message
                                                                        );
        return feedItem.Id;
    } 

    /*******************************************************************************************************
    * @description Method for sending Chatter Messages 
    * @param itemToPostTo The parent of the post. Can be a user ID, a group ID, or a record ID.
    * @param message The text of the post. You can @-mention a
     *                         user or group by using the syntax {ID}, for example:
     *                         'Hello {005x0000000URNP}, have you seen the group {0F9x00000000D7m}?'
     *                         You can include rich text by using supported HTML tags:
     *                         <b>, <i>, <u>, <s>, <ul>, <ol>, <li>, <p>.
     *                         You can include an inline image by using the syntax {img:ID} or
     *                         {img:ID:alt text}, for example: 'Have you seen this gorgeous view?
     *                         {img:069x00000000D7m:View of the Space Needle from our office.}?'
     *                         You can use links to records by {id:recordId}.
     *                         Links and hashtags will be automatically parsed if provided.
    * @return Id Id of the FeedElement that has been created
    * @example
    * COM_ChatterPublisherService chatterPublisherService = new COM_ChatterPublisherService();
    * chatterPublisherService.sendRichTextMessage('005x0000000URNP', 'Have you seen this gorgeous view?
     *                         {img:069x00000000D7m:View of the Space Needle from our office.}?');
    */
    public Id sendRichTextMessage(String itemToPostTo, String message) {
        ConnectApi.FeedItem feedItem = (ConnectApi.FeedItem)ConnectApiHelper.postFeedItemWithRichText(
                                                                                this.networkId,
                                                                                itemToPostTo,
                                                                                message
                                                                        );
        return feedItem.Id;
    } 
}
