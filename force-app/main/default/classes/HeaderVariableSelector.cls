public class HeaderVariableSelector extends fflib_SObjectSelector
{
    public List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                HeaderVariables__mdt.DeveloperName,
                HeaderVariables__mdt.MasterLabel,
                HeaderVariables__mdt.Value__c,
                HeaderVariables__mdt.Webservice__c
        };
    }

    public Schema.SObjectType getSObjectType()
    {
        return HeaderVariables__mdt.sObjectType;
    }

    public List<HeaderVariables__mdt> selectById(Set<ID> idSet)
    {
        return (List<HeaderVariables__mdt>) selectSObjectsById(idSet);
    }

}