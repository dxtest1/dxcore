/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Account
* @group-content ../../ApexDocContent/Account.htm
*
* @description Currently a showcase Trigger class for Accounts. Does not 
* implement any after or before triggers.
*/
public with sharing class COM_AccountTrigger  extends fflib_SObjectDomain {
    public COM_AccountTrigger(List<Account> sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new COM_AccountTrigger(sObjectList);
        }
    }
}
