public class COM_OpportunityTestFactory {
        private Opportunity obj;

        // AccountId is always required on an Opportunity, so are within the construct
        public COM_OpportunityTestFactory(Id accountId, String opportunityName, String recordTypeName, Date closeDate) {
            COM_RecordTypeSelector recordTypeSelector = new COM_RecordTypeSelector();

            obj = new Opportunity(
                    AccountId = accountId,
                    Name = opportunityName == null ? 'Testing the Opportunity' : opportunityName,
                    StageName = 'Qualification',
                    CloseDate = closeDate
                    //RecordTypeId = recordTypeSelector.getAllRecordTypesBySobjectType(Opportunity.sObjectType,false,false).get(recordTypeName).Id
            );
        }

		public COM_OpportunityTestFactory() {
			this(
				new COM_AccountTestFactory().save().getRecord().Id,
				'Testing',
				'',
				Date.today()
			);
		}

        public COM_OpportunityTestFactory withAmount(Decimal amount){
            obj.Amount = amount;
            return this;
        }

        public COM_OpportunityTestFactory withStageName(String stageName){
            obj.StageName = stageName;
            return this;
        }

        public COM_OpportunityTestFactory save() {
            insert obj;
            return this;
        }

        public Opportunity getRecord() {
            return obj;
        }
    }