global class COM_InvocableAutoNumber {

    @InvocableMethod(label='Auto Number field' description='Generates a new number in a requested field and overrides')
    global static List<AutoNumberResult> COM_InvocableAutoNumber(List<AutoNumberRequest> requests) {  
        List<AutoNumberResult> results = new List<AutoNumberResult>(); 
        for(AutoNumberRequest request : requests) {
            try {
                COM_AutoNumberService autoNumberService = new COM_AutoNumberService(request.autoNumberName);
                results.add(
                    new AutoNumberResult(
                        autoNumberService.giveAutoNumber(request.recordId),
                        request.recordId
                    )
                );
            }
            catch(COM_AutoNumberService.AutoNumberNotFoundException e) {
                results.add(
                    new AutoNumberResult(
                        request.recordId
                    )
                );
            }
        }
        return results;
    }

    global class AutoNumberRequest {
        @InvocableVariable(required=true)
        global ID recordId;

        @InvocableVariable(required=true)
        global String autoNumberName;
    }

    global class AutoNumberResult {
        public AutoNumberResult(String currentValue, Id recordId) {
            this.success = true;
            this.currentValue = currentValue;
            this.recordId = recordId;
        }

        public AutoNumberResult(Id recordId) {
            this.success = false;
            this.recordId = recordId;
        }

        @InvocableVariable
        global Boolean success;

        @InvocableVariable
        global String currentValue;

        @InvocableVariable
        global ID recordId;
    }
}
