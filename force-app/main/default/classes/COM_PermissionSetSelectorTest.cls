@isTest
public with sharing class COM_PermissionSetSelectorTest {
    
    // Testing getting Permission Set by name, all of the tests test the getAll
    @isTest()
    static void test_getPermissionByName() {
        COM_PermissionSetSelector selector = new COM_PermissionSetSelector();
        List<PermissionSet> all_perm = selector.getAll();

        // We give our selfes that a single permission set exists
        PermissionSet permission_set_used = all_perm[0];

        // Test getting name
        PermissionSet permission_set_single_by_name = selector.selectOneByName(permission_set_used.Name);

        // Test getting by sets of name
        List<PermissionSet> permission_set_multiple_by_name = selector.selectByName(new Set<String> { permission_set_used.Name });

        // Check if it's the same id coming back

        System.AssertEquals(permission_set_used.Id, permission_set_single_by_name.Id);
        System.AssertEquals(permission_set_used.Id, permission_set_multiple_by_name[0].Id);
    }

    @isTest()
    static void test_getPermissionByLabel() {
        COM_PermissionSetSelector selector = new COM_PermissionSetSelector();
        List<PermissionSet> all_perm = selector.getAll();

        // We give our selfes that a single permission set exists
        PermissionSet permission_set_used = all_perm[0];

        // Test getting name
        PermissionSet permission_set_single_by_name = selector.selectOneByLabel(permission_set_used.Label);

        // Test getting by sets of name
        List<PermissionSet> permission_set_multiple_by_name = selector.selectByLabel(new Set<String> { permission_set_used.Label });

        // Check if it's the same id coming back

        System.AssertEquals(permission_set_used.Id, permission_set_single_by_name.Id);
        System.AssertEquals(permission_set_used.Id, permission_set_multiple_by_name[0].Id);
    }

    @isTest()
    static void test_getPermissionById() {
        COM_PermissionSetSelector selector = new COM_PermissionSetSelector();
        List<PermissionSet> all_perm = selector.getAll();

        // We give our selfes that a single permission set exists
        PermissionSet permission_set_used = all_perm[0];

        // Test getting by sets of name
        List<PermissionSet> permission_set_multiple_by_name = selector.selectById(new Set<Id> { permission_set_used.Id });

        // Check if it's the same id coming back
        System.AssertEquals(permission_set_used.Id, permission_set_multiple_by_name[0].Id);
    }
}
