public with sharing class COM_Helper {

    public static List<COM_OptionsModel> getPicklistForOptionsModels(Sobject object_name, String field_name) {
        List<COM_OptionsModel> options = new List<COM_OptionsModel>();

        Schema.sObjectType sobject_type = object_name.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
            options.add(new COM_OptionsModel(a.getLabel(), a.getValue()));
        }
        return options;
    }

    public static List<String> getPicklist(Sobject object_name, String field_name, String first_val) {
        List<String> options = new List<String>();
        if (first_val != null) {
            options.add(first_val);
        }
        Schema.sObjectType sobject_type = object_name.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
            options.add(a.getValue());
        }
        return options;
    }


    public static Map<Id, List<sObject>> listToMapList(String relatedField, List<sObject> recordlist) {
        Map<Id, List<sObject>> recordMap = new Map<Id, List<sObject>>();
        for(sObject record : recordlist) {
            if(recordMap.containsKey((Id)record.get(relatedField))) {
                  List<sObject> actionList = recordMap.get((Id)record.get(relatedField));
                  actionList.add(record);
                  recordMap.put((Id)record.get(relatedField), actionList);
            } else {
                recordMap.put((Id)record.get(relatedField), new List<sObject> {record});
            }
        }
        return recordMap;
    }

    public static Set<Id> list2SetOfIds(list<sObject> objList) {

        return (new Map<Id, SObject>(objList)).keySet(); 
    } 

    /*
    * Method to get Set of Id for given sObject which store the ids in given field.
    */
    public static Set<Id> list2SetOfIds(list<sObject> recordlist, String fieldNameWithId) {
        Set<Id> setOfIds = new Set<Id>();

        for(sObject record : recordlist) {
            setOfIds.add( (Id)record.get(fieldNameWithId) );
        }

        return setOfIds; 
    } 

    /*
    * Method to get list of the COM_OptionsModel from the PicklistValues__c object which has to be filled before by the COM_UIAPIPicklistScheduleJob.
    */
    public static List<COM_OptionsModel> getPicklistForOptionsModels(String sObjectName, String fieldName, String recordTypeId) {
        String key = COM_UIAPIPicklistService.getMapKey(sObjectName, fieldName, recordTypeId); 
        PicklistValues__c picklistValues = new COM_PicklistValuesSelector().selectOneByKey(key);

        List<COM_OptionsModel> options = new List<COM_OptionsModel>();
        if(picklistValues != null) {
            options = (List<COM_OptionsModel>)JSON.deserialize(picklistValues.values__c, List<COM_OptionsModel>.class);
        }

        return options;
    }

}