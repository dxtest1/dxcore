/**
* @author David Fannar Gunnarsson
* @date 2019-10-16
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description A Common PermissionSetSelector class built on the fflib ( Apex Enterprise Patterns )
* selector library. It implements fflib_SObjectSelector and is a virtual class so it's extendable by
* application specific classes.
*/
public virtual class COM_PermissionSetSelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				PermissionSet.Id,
				PermissionSet.Name,
                PermissionSet.Label,
                PermissionSet.NamespacePrefix,
                PermissionSet.ProfileId,
                PermissionSet.Description
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return PermissionSet.sObjectType;
	}

    public List<PermissionSet> getAll() {
        return (List<PermissionSet>) Database.query(newQueryFactory().toSOQL());
    }

	public List<PermissionSet> selectById(Set<ID> idSet)
	{
		return (List<PermissionSet>) selectSObjectsById(idSet);
	}	

	public List<PermissionSet> selectByName(Set<String> names)
	{
		return (List<PermissionSet>) Database.query(newQueryFactory().setCondition('NAME IN :names').toSOQL());
	}

    public PermissionSet selectOneByName(String name) {
        List<PermissionSet> getRecord = this.selectByName(new Set<String> { name });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }
    
    public List<PermissionSet> selectByLabel(Set<String> labels)
	{
		return (List<PermissionSet>) Database.query(newQueryFactory().setCondition('Label IN :labels').toSOQL());
	}

    public PermissionSet SelectOneByLabel(String label) {
        List<PermissionSet> getRecord = this.selectByLabel(new Set<String> { label });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }

}