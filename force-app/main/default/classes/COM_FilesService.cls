public with sharing class COM_FilesService {

    /*
    * Method to save File as a ContentVersion. Filename should be unique to update ContentVersion in case of multiple saves.
    * inspiration: //https://stackoverflow.com/questions/47225683/error-while-updating-versiondata-field-of-contentversion-object
    */
    public static ContentVersion saveFileAsContentVersion(string filename, String fileTitle, Blob blobData, Id linkedEntityId){
        COM_ContentVersionSelector contentVersionSelector = new COM_ContentVersionSelector();

        ContentVersion newContentVersion = new ContentVersion();
        list <ContentVersion> flist = contentVersionSelector.selectByFileName(filename);

        if ( flist.size() > 0 ){
            newContentVersion = flist[0];
            newContentVersion.VersionData = blobData;
            update newContentVersion;
        } else {
            newContentVersion = new ContentVersion(title = fileTitle, PathOnClient = filename, VersionData = blobData,IsMajorVersion = false) ;
            insert newContentVersion;

            Id contentDocumentId = contentVersionSelector.selectOneById(newContentVersion.Id).ContentDocumentId;
            insert new ContentDocumentLink(   LinkedEntityId = linkedEntityId, ContentDocumentId = contentDocumentId, ShareType = 'I');
        }
        return newContentVersion;
    } 
}
