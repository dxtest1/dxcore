public virtual class COM_ProfileSelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				Profile.Id,
				Profile.Name
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return Profile.sObjectType;
	}

	public List<Profile> selectById(Set<ID> idSet)
	{
		return (List<Profile>) selectSObjectsById(idSet);
	}	

	public List<Profile> selectByName(Set<String> names)
	{
		System.debug(names);
		return (List<Profile>) Database.query(newQueryFactory().setCondition('NAME IN :names').toSOQL());
	}

    public Profile selectOneByName(String name) {
        List<Profile> getRecord = this.selectByName(new Set<String> { name });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }
    
}