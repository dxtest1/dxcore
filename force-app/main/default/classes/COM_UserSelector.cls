public virtual class COM_UserSelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				User.Id,
				User.Name,
				User.FirstName,
				User.LastName,
				User.Username,
				User.Email,
				User.Alias,
				User.CommunityNickname,
				User.TimeZoneSidKey,
				User.CurrencyIsoCode,
				User.LocaleSidKey,
				User.EmailEncodingKey,
				User.ProfileId,
				User.LanguageLocaleKey,
				User.FederationIdentifier
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return User.sObjectType;
	}

    public List<User> getAll() {
        return (List<User>) Database.query(newQueryFactory().toSOQL());
    }

	public User selectOneById(Id idOfRecord) {
		List<User> getRecord = this.selectById(new Set<Id>{
				idOfRecord
		});
        return getRecord.size() >  0 ? getRecord[0] : null;
	}

	public List<User> selectById(Set<Id> idSet)
	{
		return (List<User>) selectSObjectsById(idSet);
	}	


	public List<User> selectByUsername(Set<String> usernames)
	{
		return (List<User>) Database.query(newQueryFactory().setCondition('Username IN :usernames').toSOQL());
	}

    public User selectOneByUsername(String username) {
        List<User> getRecord = this.selectByUsername(new Set<String> { username });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }

	public List<User> selectWithPermissionSetAssignments(Set<Id> idSet) {

        fflib_QueryFactory userQueryFactory = newQueryFactory();

		new COM_PermissionSetAssignmentSelector().
				addQueryFactorySubselect(userQueryFactory, 'PermissionSetAssignments');

        return (List<User>) Database.query(
                userQueryFactory.setCondition('id in :idSet').toSOQL());

    }
}