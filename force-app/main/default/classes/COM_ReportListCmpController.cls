public with sharing class COM_ReportListCmpController {

    @TestVisible private static COM_ReportSelector selector = new COM_ReportSelector();

    @AuraEnabled(Cacheable=true)
    public static List<ReportWrapper> selectReportsByFolderDeveloperName(String folderName) {
        List<ReportWrapper> reportWrappers = new List<ReportWrapper>();
        for(Report r : selector.selectByFolderDeveloperName(folderName)){
            reportWrappers.add(new ReportWrapper(r));
        }
        return reportWrappers;
    }

    public class ReportWrapper {
        @AuraEnabled public String reportUrl;
        @AuraEnabled public String reportLabel;
        @AuraEnabled public String reportName;

        public ReportWrapper(Report r){
            reportUrl = '/' + r.Id;
            reportLabel = r.Name;
            reportName = r.DeveloperName;
        }
    }

}