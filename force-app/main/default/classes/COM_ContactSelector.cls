/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Contact
* @group-content ../../ApexDocContent/Contact.htm
*
* @description Selector class for Contacts, which offers access patterns for the
* Contacts. All Access patterns to this objects should be defined here and direct
* SOQL queries in Service class should be avoided.
*/
public virtual class COM_ContactSelector extends fflib_SObjectSelector
{
    public virtual List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Contact.Id,
                Contact.Name,
                Contact.AccountId,
                Contact.CurrencyIsoCode
        };
    }

    public Schema.SObjectType getSObjectType()
    {
        return Contact.sObjectType;
    }

    public List<Contact> getAll() {
        return (List<Contact>) Database.query(newQueryFactory().toSOQL());
    }

    public Contact selectOneById(ID idOfRecord) {
        List<Contact> get_Contact = this.selectById(new Set<Id> { idOfRecord });
        return get_Contact.size() > 0 ? get_Contact[0] : null;
    }

    public List<Contact> selectById(Set<ID> idSet)
    {
        return (List<Contact>) selectSObjectsById(idSet);
    }

}