/**
* @author David Fannar Gunnarsson
* @date 2019-10-16
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description A Common PicklistValuesSelector class built on the fflib ( Apex Enterprise Patterns )
* selector library. It implements fflib_SObjectSelector and is a virtual class so it's extendable by
* application specific classes.
*/
public virtual class COM_PicklistValuesSelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				PicklistValues__c.Id,
                PicklistValues__c.ObjNameFieldRecordTypeIdKey__c,
                PicklistValues__c.values__c
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return PicklistValues__c.sObjectType;
	}

    public List<PicklistValues__c> getAll() {
        return (List<PicklistValues__c>) Database.query(newQueryFactory().toSOQL());
    }

	public List<PicklistValues__c> selectById(Set<ID> idSet)
	{
		return (List<PicklistValues__c>) selectSObjectsById(idSet);
	}	

	public List<PicklistValues__c> selectByKey(Set<String> keys)
	{
		return (List<PicklistValues__c>) Database.query(newQueryFactory().setCondition(PicklistValues__c.ObjNameFieldRecordTypeIdKey__c.getDescribe().getName() + ' IN :keys').toSOQL());
	}

    public PicklistValues__c selectOneByKey(String key) {
        List<PicklistValues__c> get_PicklistValues = this.selectByKey(new Set<String> { key });
        return get_PicklistValues.size() > 0 ? get_PicklistValues[0] : null;
    }
}