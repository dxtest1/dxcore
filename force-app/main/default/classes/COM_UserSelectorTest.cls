@isTest
public with sharing class COM_UserSelectorTest {

    // Testing getting User by Id, all of the tests test the getAll
    @isTest()
    static void test_getUserById() {
        COM_UserSelector selector = new COM_UserSelector();
        User user_selected = new COM_UserTestFactory('System Administrator').save().getRecord();
        Id userId = user_selected.Id;
        // Test getting name
        User user_single = selector.selectOneById(userId);

        // Test getting by sets of name
        List<User> user_multiple = selector.selectById(new Set<Id> { userId });

        // Check if it's the same id coming back
        
        System.AssertEquals(userId, user_multiple[0].Id);
        System.AssertEquals(userId, user_single.Id);
    }

    @isTest()
    static void test_getUserByUsername() {
        COM_UserSelector selector = new COM_UserSelector();
        User user_selected = new COM_UserTestFactory('System Administrator').save().getRecord();

        // Test getting name
        User user_single = selector.selectOneByUsername(user_selected.Username);

        // Test getting by sets of name
        List<User> user_multiple = selector.selectByUsername(new Set<String> { user_selected.Username });

        // Check if it's the same id coming back
        System.AssertEquals(user_selected.Id, user_single.Id);
        System.AssertEquals(user_selected.Id, user_multiple[0].Id);
    }
}
