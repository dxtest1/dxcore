
/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Account
* @group-content ../../ApexDocContent/Account.htm
*
* @description Test COM_AccountSelector, it's currently only testing to get code coverage
* and has little asseretions.
*/
@isTest()
private class COM_AccountSelectorTest {
    @testSetup()
    static void setup() {
        COM_AccountTestFactory accountBuilder = new COM_AccountTestFactory();
        accountBuilder.save();

        COM_OpportunityTestFactory oppBuilder = new COM_OpportunityTestFactory(accountBuilder.getRecord().Id, 'Test', 'Capital', Date.today());
        oppBuilder.save();
    }

    @isTest()
    static void test_getAccountById() {
    
        // Select all Accounts available ( should be one )
        COM_AccountSelector accountSelector = new COM_AccountSelector();

        List<Account> account = accountSelector.getAll();

        // Select one by Id 
        accountSelector.selectOneById(account[0].Id);

        // Select one by choosing a set of Id
        accountSelector.selectById(new Set<Id>{account[0].Id});

        accountSelector.selectOneByName(account[0].Name);

        accountSelector.selectByName(new Set<String>{account[0].Name});
    }

    @isTest()
    static void test_getOpportunityId() {
    
        // Select all Accounts available ( should be one )
        COM_AccountSelector accountSelector = new COM_AccountSelector();
        COM_OpportunitySelector opportunityselector = new COM_OpportunitySelector();
        List<Opportunity> opportunity = opportunityselector.getAll();
        List<Account> account = accountSelector.selectByOpportunity(opportunity);

    }
}
