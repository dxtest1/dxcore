/**
* @author David Fannar Gunnarsson
* @date 2019-10-16
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description Class to test a COM_UIAPIModel class.
*/
@IsTest
public class COM_UIAPIModelTest {
	
	static testMethod void testParse() {
		String json = '{\"controllerValues\":{},\"defaultValue\":null,\"url\":\"/services/data/v41.0/ui-api/object-info/Opportunity/picklist-values/0127E000000dfGkQAI/Amputation_Level__c\",\"values\":[{\"attributes\":null,\"label\":\"Shoulder Disarticulation\",\"validFor\":[],\"value\":\"Shoulder Disarticulation\"},{\"attributes\":null,\"label\":\"Above Elbow Disarticulation\",\"validFor\":[],\"value\":\"Above Elbow Disarticulation\"},{\"attributes\":null,\"label\":\"Below Elbow Disarticulation\",\"validFor\":[],\"value\":\"Below Elbow Disarticulation\"},{\"attributes\":null,\"label\":\"Wrist Disarticulation\",\"validFor\":[],\"value\":\"Wrist Disarticulation\"},{\"attributes\":null,\"label\":\"Partial Hand\",\"validFor\":[],\"value\":\"Partial Hand\"},{\"attributes\":null,\"label\":\"Lower Limb\",\"validFor\":[],\"value\":\"Lower Limb\"},{\"attributes\":null,\"label\":\"Below Knee\",\"validFor\":[],\"value\":\"Below Knee\"},{\"attributes\":null,\"label\":\"Transmet\",\"validFor\":[],\"value\":\"Transmet\"},{\"attributes\":null,\"label\":\"Partial Foot\",\"validFor\":[],\"value\":\"Partial Foot\"},{\"attributes\":null,\"label\":\"Unknown\",\"validFor\":[],\"value\":\"Unknown\"}]}';
		COM_UIAPIModel obj = COM_UIAPIModel.parse(json);
		System.assert(obj != null);
	}
}