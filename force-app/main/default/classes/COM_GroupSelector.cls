public class COM_GroupSelector extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Group.Id,
            Group.DeveloperName
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Group.sObjectType;
    }

    public List<Group> selectQueuesByDeveloperName(Set<String> names) {
        return (List<Group>) Database.query(
            newQueryFactory()
                .setCondition('DeveloperName in :names AND Type = \'Queue\'')
                .toSOQL()
        );
    }

    public Group selectQueueByDeveloperName(String name) {
        List<Group> records = this.selectQueuesByDeveloperName(new Set<String> { name });
        return records.isEmpty() ? null : records.get(0);
    }
}