/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Account
* @group-content ../../ApexDocContent/Account.htm
*
* @description The Selector class for Accounts. It's the Common selector which all
* other selectors depend upon.
*/
public virtual class COM_AccountSelector extends fflib_SObjectSelector
implements COM_IAccountSelector
{
    public virtual List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Account.Id,
                Account.Name,
                Account.Description,
                Account.ShippingPostalCode,
                Account.ShippingStreet,
                Account.ShippingStateCode,
                Account.ShippingCountryCode,
                Account.ShippingCity,
                Account.BillingPostalCode,
                Account.BillingStreet,
                Account.BillingStateCode,
                Account.BillingCountryCode,
                Account.BillingCity,
                Account.CurrencyIsoCode,
                Account.Website,
                Account.Phone,
                Account.Fax,
                Account.MasterRecordId,
                Account.ParentId
        };
    }

    public Schema.SObjectType getSObjectType()
    {
        return Account.sObjectType;
    }

    public List<Account> getAll() {
        return (List<Account>) Database.query(newQueryFactory().toSOQL());
    }

    public Account selectOneById(ID idOfRecord) {
        List<Account> get_account = this.selectById(new Set<Id> { idOfRecord });
        return get_account.size() > 0 ? get_account[0] : null;
    }

    public List<Account> selectById(Set<ID> idSet)
    {
        return (List<Account>) selectSObjectsById(idSet);
    }

    public Account selectOneByName(String name) {
        List<Account> get_account = this.selectByName(new Set<String> { name });
        return get_account.size() > 0 ? get_account[0] : null;
    }

    public List<Account> selectByName(Set<String> nameSet)
    {
        return (List<Account>) Database.query(
                                                newQueryFactory().
                                                    setCondition(Account.Name.getDescribe().getName() + ' IN :nameSet').
                                                    toSOQL()
                                                );
    }

    public List<Account> selectByOpportunity(List<Opportunity> opportunities)
    {
        // Related Accounts
        Set<Id> accountIds = new Set<Id>();
        for(Opportunity opp : (List<Opportunity>) opportunities)
            if(opp.AccountId!=null)
                accountIds.add(opp.AccountId);

        return selectById(accountIds);
    }

}