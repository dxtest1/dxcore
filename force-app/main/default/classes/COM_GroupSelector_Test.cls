@isTest()
private class COM_GroupSelector_Test {
    @testSetup()
    static void setup() {
        insert new Group(
            DeveloperName = 'COM_GroupSelector_testQueue',
            Name = 'test queue',
            Type = 'Queue'
        );
    }

    @isTest()
    static void test_selectQueueByDeveloperName() {
        COM_GroupSelector selector = new COM_GroupSelector();

        Group result = selector.selectQueueByDeveloperName('COM_GroupSelector_testQueue');

        System.assertNotEquals(null, result);
        System.assertEquals('COM_GroupSelector_testQueue', result.DeveloperName);
    }
}