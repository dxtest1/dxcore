@isTest
private class HttpCalloutServiceTest {

	@isTest static void sendGetRequest_codeError() {
		String endpoint = 'https://test.test.com/test/test';
		HttpCalloutService cal = new HttpCalloutService(endpoint);
		cal.addUrlParam('Test', 'Testing');
		cal.addHeaderLine('Test', 'Testing');

		//mock setup
		Integer code = 404;
		Test.setMock(HttpCalloutMock.class, new HttpCalloutServiceMock(code, null, null, null));

		Boolean expectedExceptionThrown =  false;

		Test.startTest();
		try {
			HTTPResponse res = cal.sendGetRequest();
		} catch(Exception e) {
			expectedExceptionThrown =  e.getMessage().contains('Failed to recieve a success code from remote. Code was: 404') ? true : false;
		}
		Test.stopTest();

		System.AssertEquals(expectedExceptionThrown, true);
	}

	@isTest static void sendGetRequest_code200() {
		String endpoint = 'https://test.test.com/test/test';
		HttpCalloutService cal = new HttpCalloutService(endpoint);
		cal.addUrlParam('Test', 'Test');
		cal.addHeaderLine('Test', 'Test');

		//mock setup
		Integer code = 200;
		Test.setMock(HttpCalloutMock.class, new HttpCalloutServiceMock(code, null, null, null));

		Test.startTest();
		HTTPResponse res = cal.sendGetRequest();
		Test.stopTest();

		String expectedEndpoint = 'System.HttpRequest[Endpoint=https://test.test.com/test/test?Test=Test, Method=GET]';
		System.assertEquals(code, res.getStatusCode());
		System.assertEquals(expectedEndpoint, res.getBody());
	}


	
}