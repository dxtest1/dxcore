/**
* @author David Fannar Gunnarsson
* @date 2019-10-16
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description a common model class to model a response from a UI API with picklist values
*/
public class COM_UIAPIModel {

	public COM_OptionsModel defaultValue;
	public List<COM_OptionsModel> values;
	
	public static COM_UIAPIModel parse(String json) {
		String jsonWithoutAmp = json.replace('&amp;', '&');
		return (COM_UIAPIModel) System.JSON.deserialize(jsonWithoutAmp, COM_UIAPIModel.class);
	}
}