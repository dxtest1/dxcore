/**
* @author David Fannar Gunnarsson
* @date 2019-10-16
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description a common schedule job to populate/update/delete the PicklistValues__c records base on the UIAPIPicklistsToFetch__mdt add the UI API calls.
*/
global with sharing class COM_UIAPIPicklistScheduleJob implements Schedulable  {

    global void execute(SchedulableContext ctx) {
           fetchPicklistValuesAndUpdate();
    }

    @future(callout=true) //Because http callouts are not allowed in the Schedulable
    public static void fetchPicklistValuesAndUpdate(){
       Map<String, String> newPicklistValuesMap = COM_UIAPIPicklistService.getNewPicklistValuesFromUIAPIMap();
       
       COM_UIAPIPicklistService.commitNewPicklistValues(newPicklistValuesMap);
    }
}
  
