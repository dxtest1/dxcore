
public virtual class COM_UIAPIPicklistsToFetchSelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				UIAPIPicklistsToFetch__mdt.Id,
				UIAPIPicklistsToFetch__mdt.sObjectName__c,
				UIAPIPicklistsToFetch__mdt.fieldName__c
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return UIAPIPicklistsToFetch__mdt.sObjectType;
	}

    public List<UIAPIPicklistsToFetch__mdt> selectAll() {
        return (List<UIAPIPicklistsToFetch__mdt>) Database.query(newQueryFactory().toSOQL());
    }
}