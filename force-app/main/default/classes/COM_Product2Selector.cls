public virtual class COM_Product2Selector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				Product2.Id,
				Product2.Name
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return Product2.sObjectType;
	}

	public List<Product2> selectById(Set<ID> idSet)
	{
		return (List<Product2>) selectSObjectsById(idSet);
	}	

}