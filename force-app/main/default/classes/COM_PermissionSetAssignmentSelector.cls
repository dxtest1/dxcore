/**
* @author David Fannar Gunnarsson
* @date 2019-10-16
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description A Common COM_PermissionSetAssignmentSelector class built on the fflib ( Apex Enterprise Patterns )
* selector library. It implements fflib_SObjectSelector and is a virtual class so it's extendable by
* application specific classes.
*/
public virtual class COM_PermissionSetAssignmentSelector extends fflib_SObjectSelector
{
	public virtual List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				PermissionSetAssignment.Id,
				PermissionSetAssignment.AssigneeId,
                PermissionSetAssignment.PermissionSetId
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return PermissionSetAssignment.sObjectType;
	}

    public override String getOrderBy()
    {
        return 'SystemModstamp DESC';
    }

    public List<PermissionSetAssignment> getAll() {
        return (List<PermissionSetAssignment>) Database.query(newQueryFactory().toSOQL());
    }

	public List<PermissionSetAssignment> selectById(Set<ID> idSet)
	{
		return (List<PermissionSetAssignment>) selectSObjectsById(idSet);
	}	

    
}