@isTest
public class HttpCalloutServiceMock implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    private static Integer executionCounter = 0;

    public HTTPResponse respond; //for testing

    public HttpCalloutServiceMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {
        

        System.debug('HTTPResponse respond executionCounter: ' + executionCounter++);


        HttpResponse res = new HttpResponse();

        if(this.code != null) {
            res.setStatusCode(this.code);
        } else {
            res.setStatusCode(200);
        }

        if(this.responseHeaders != null) {
            for (String key : this.responseHeaders.keySet()) {
                res.setHeader(key, this.responseHeaders.get(key));
            }
        }

        if(this.status != null) {
            res.setStatus(this.status);
        }

        if(this.body != null) {
            res.setBody(this.body);
        } else {
            res.setBody(req.toString());
        }   

        this.respond = res;
        return res;
    }

}