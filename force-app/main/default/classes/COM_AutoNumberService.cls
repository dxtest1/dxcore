public class COM_AutoNumberService {
    public class AutoNumberNotFoundException extends Exception {}

    private COM_AutoNumber__c autoNumberInstance;
    public COM_AutoNumberService(String autoNumberName) {
        COM_AutoNumberSelector autonumberSelector = new COM_AutoNumberSelector();
        this.autoNumberInstance = autonumberSelector.selectOneByName(autoNumberName);
        if(autoNumberInstance == null) {
            throw new AutoNumberNotFoundException(
                'Can\'t find the specified autonumber object. Check if the autonumber instance exists'
            );
        }
    }

    public String giveAutoNumber(Id recordId) {
        /* 
         * This is not in a selector, since this completely out of a domain
         * the object getting selected is dynamic from the autonumberSelector
         */
        SObject updatedSObject = Database.query('SELECT ' + this.autoNumberInstance.Field__c + ' FROM ' + this.autoNumberInstance.Entity__c + ' WHERE Id = \'' + recordId + '\' LIMIT 1');
        
        String counter = '';
        if(
            this.autoNumberInstance.Auto_Number_Prefix__c != null 
            && this.autoNumberInstance.Auto_Number_Prefix__c.length() > 0
        ) 
        {
            counter += this.autoNumberInstance.Auto_Number_Prefix__c + '-';
        }
        counter += String.valueOf(this.autoNumberInstance.Counter__c);

        this.autoNumberInstance.Counter__c += 1;
        update this.autoNumberInstance;

        updatedSObject.put(this.autoNumberInstance.Field__c, counter);
        update updatedSObject;

        return counter;
    }
}
