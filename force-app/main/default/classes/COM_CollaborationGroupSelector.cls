/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description Selector class for CollabrationGroup, which offers access patterns for the
* CollabrationGroup. All Access patterns to this objects should be defined here and direct
* SOQL queries in Service class should be avoided.
*/
public virtual class COM_CollaborationGroupSelector extends fflib_SObjectSelector{

	public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
				CollaborationGroup.Id,
                CollaborationGroup.Name
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return CollaborationGroup.sObjectType;
	}


	public List<CollaborationGroup> selectByNames(Set<String> names) {
		return (List<CollaborationGroup>) Database.query(
													newQueryFactory()
													.setCondition(CollaborationGroup.Name.getDescribe().getName() + ' IN :names')
													.toSOQL());
	}

    public CollaborationGroup selectOneByName(String name) {
        List<CollaborationGroup> collaborationGroups = this.selectByNames(new Set<String> { name });
        return collaborationGroups.size() > 0 ? collaborationGroups[0] : null;
    }

}