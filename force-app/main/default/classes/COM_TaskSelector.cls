public with sharing class COM_TaskSelector extends fflib_SObjectSelector {
	public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
				Task.Id,
                Task.WhatId,
                Task.Status,
                Task.Priority,
                Task.Description,
                Task.Subject,
				Task.WhoId,
				Task.ActivityDate,
				Task.OwnerId
			};
	}

	public Schema.SObjectType getSObjectType() {
		return Task.sObjectType;
	}

	public List<Task> selectById(Set<ID> idSet) {
		return (List<Task>) selectSObjectsById(idSet);
	}	

    public Task selectOneById(ID idOfRecord) {
        List<Task> getRecord = this.selectById(new Set<Id> { idOfRecord });
        return getRecord.size() > 0 ? getRecord[0] : null;
    }

	public List<Task> selectByWhatId(Set<ID> idSetWhatId) {
		return (List<Task>) Database.query(
										        newQueryFactory().
											        setCondition(Task.WhatId + ' IN :idSetWhatId').
											        toSOQL()
											    );
	}
}