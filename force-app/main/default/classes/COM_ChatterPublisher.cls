/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group Common
* @group-content ../../ApexDocContent/Common.htm
*
* @description Invocable method to be able to mention people in Chatter when invoking through Process Builder
* the reason for it exists is because this method allow to post to Chatter and Mention people and other groups
* which is not possible in Flows / Process Builders overall.
*/
global class COM_ChatterPublisher {

    @InvocableMethod(label='Post to Chatter' description='This method allows to post to Chatter and @Mention people. Format for mention is {Id}')
    global static List<FeedItemResult> publistToChatter(List<FeedItemRequest> requests) {
            List<FeedItemResult> results = new List<FeedItemResult>();
            for (FeedItemRequest request : requests) {
                COM_ChatterPublisherService chatterPublisherService = new COM_ChatterPublisherService(request.networkId);
                results.add(new FeedItemResult(chatterPublisherService.sendMessage(request.itemToPostTo, request.message)));
            }
            return results;
    }

    global class FeedItemRequest {
        @InvocableVariable
        global Id networkId;

        @InvocableVariable
        global String itemToPostTo;

        @InvocableVariable
        global String message;
    }

    global class FeedItemResult {
        public FeedItemResult(Id feedItemId) {
            this.feedItemId = feedItemId;
        }

        @InvocableVariable
        global ID feedItemId;
    }

}