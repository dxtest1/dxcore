public virtual class WebserviceSettingSelector extends fflib_SObjectSelector
{
    public List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                WebserviceSettings__mdt.DeveloperName,
                WebserviceSettings__mdt.MasterLabel,
                WebserviceSettings__mdt.WebserviceURL__c
        };
    }

    public Schema.SObjectType getSObjectType()
    {
        return WebserviceSettings__mdt.sObjectType;
    }

    public List<WebserviceSettings__mdt> selectById(Set<ID> idSet)
    {
        return (List<WebserviceSettings__mdt>) selectSObjectsById(idSet);
    }

    /* Select Webservices with the Header Variables inside for the DeveloperName */
    public List<WebserviceSettings__mdt> selectWithHeadersByDeveloperName(Set<String> DNSet) {

        fflib_QueryFactory webServiceFactory = newQueryFactory();
        new HeaderVariableSelector().
                addQueryFactorySubselect(webServiceFactory, 'Header_variables__r');

        return (List<WebserviceSettings__mdt>) Database.query(
                webServiceFactory.setCondition('DeveloperName in :DNSet').toSOQL());
    }

    public WebserviceSettings__mdt selectOneWithHeadersByDeveloperName(String DeveloperName) {
        List<WebserviceSettings__mdt> get_ws = this.selectWithHeadersByDeveloperName(new Set<String> { DeveloperName });
        return get_ws.size() > 0 ? get_ws[0] : null;
    }

}