/**
* @author David Fannar Gunnarsson
* @date 2019-11-01
*
* @group AutoNumber
* @group-content ../../ApexDocContent/AutoNumber.htm
*
* @description AutoNumber Test Factory offers an easy way to implement
* a AutoNumber into other tests by offering easy access patterns
*/
@isTest()
public class COM_AutoNumberTestFactory {
        private COM_AutoNumber__c obj;

        public COM_AutoNumberTestFactory(String Name, String Entity, String Field) {
            obj = new COM_AutoNumber__c( 
                Counter__c=1,
                Entity__c=Entity,
                Field__c=Field,
                Name=Name
            );
        }

        public COM_AutoNumberTestFactory() {
            this(
                'Test',
                'Account',
                'Name'
            );
        }

        public COM_AutoNumberTestFactory updatePrefix(String prefix) {
            obj.Auto_Number_Prefix__c = prefix;
            return this;
        }
        
        public COM_AutoNumberTestFactory save() {
            if(obj.Id != null) {
                update obj;
            }
            else {
                insert obj;
            }
            return this;
        }

        public COM_AutoNumber__c getRecord() {
            return obj;
        }
}