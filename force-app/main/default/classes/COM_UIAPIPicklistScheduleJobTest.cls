@isTest
public with sharing class COM_UIAPIPicklistScheduleJobTest {

    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    private static String standardUIAPIPicklistBody = '{\"defaultValue\":null,\"url\":\"/services/data/v41.0/ui-api/object-info/Opportunity/picklist-values/0127E000000dfGkQAI/Amputation_Level__c\",\"values\":';
    private static String initialPicklistValues = '[{\"value\":\"Unknown\",\"label\":\"Unknown\"}]';

    @testSetup static void testSetup() {
        
        //mock setup
		Integer code = 200;
        String body = standardUIAPIPicklistBody + initialPicklistValues + '}';
		HttpCalloutServiceMock mock = new HttpCalloutServiceMock(code, null, body, null);
		Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
        COM_UIAPIPicklistScheduleJob.fetchPicklistValuesAndUpdate();
        Test.stopTest();
    }


    /*
    * Test if method Schedule Job COM_UIAPIPicklistScheduleJobTest create PicklistValues records.
    */
    @isTest
    public static void testScheduleJob_basic() {
        //mock setup
		Integer code = 200;
        String body = standardUIAPIPicklistBody + initialPicklistValues + '}';
		HttpCalloutServiceMock mock = new HttpCalloutServiceMock(code, null, body, null);
		Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
        String jobId = System.schedule('COM_UIAPIPicklist_test', CRON_EXP, new COM_UIAPIPicklistScheduleJob());
        Test.stopTest();

        List<PicklistValues__c> picklistValuesLlist = new COM_PicklistValuesSelector().getAll();
        for(PicklistValues__c picklistValue : picklistValuesLlist) {
            assertPicklistValues(initialPicklistValues, picklistValuesLlist[0].values__c);
        }

    }

    /*
    * Test if method Schedule Job COM_UIAPIPicklistScheduleJobTest update PicklistValues records.
    */
    @isTest
    public static void testScheduleJob_UpdateRecords() {
        String picklistValues = '[{\"value\":\"New\",\"label\":\"New\"}]';

        //mock setup
		Integer code = 200;
        String body = standardUIAPIPicklistBody + picklistValues + '}';
		HttpCalloutServiceMock mock = new HttpCalloutServiceMock(code, null, body, null);
		Test.setMock(HttpCalloutMock.class, mock);

        
        List<PicklistValues__c> picklistValuesLlistBefore = new COM_PicklistValuesSelector().getAll();

        Test.startTest();
        COM_UIAPIPicklistScheduleJob.fetchPicklistValuesAndUpdate();
        Test.stopTest();

        List<PicklistValues__c> picklistValuesLlist = new COM_PicklistValuesSelector().getAll();
        System.assertEquals(picklistValuesLlist.size(), picklistValuesLlistBefore.size());

        for(PicklistValues__c picklistValue : picklistValuesLlist) {
            assertPicklistValues(picklistValues, picklistValuesLlist[0].values__c);
        }

    }


    private static void assertPicklistValues(String expectedJSON, String actualJSON) {
        COM_OptionsModel actual = ((List<COM_OptionsModel>)JSON.deserialize(actualJSON, List<COM_OptionsModel>.class))[0];
        COM_OptionsModel expected = ((List<COM_OptionsModel>)JSON.deserialize(expectedJSON, List<COM_OptionsModel>.class))[0];

        System.assertEquals(expected.label, actual.label, 'Picklist label not set properly');
        System.assertEquals(expected.value, actual.value, 'Picklist value not set properly');
    }
}
  