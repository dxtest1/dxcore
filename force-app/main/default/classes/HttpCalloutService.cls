/**
* @author David Fannar Gunnarsson
* @date 2019-10-16
*
* @group General
* @group-content ../../ApexDocContent/General.htm
*
* @description Service class to handle generalized HttpCallouts format of the classes taken for the requests library
* of python ( requests for human ). This class should try to keep in standard with how Requests for Human does it's
* styling
*/
public virtual class HttpCalloutService {
	public String endpoint {get;set;}
    public String contentType {get;set;}
	public Map<String, String> headers {get;set;}
	public Map<String, String> params {get;set;}
	public class HTTPResultCodeException extends Exception {}
	public Integer TIMEOUT = 60000;
    public String lastDebugMessage;
    public Boolean DEBUG = false;
	private Boolean convertParamstoQueryString = false;
	private Boolean callingOwnSalesforce = false;
	private Boolean handleOwnException = false;

	public HttpCalloutService() {
		this.headers = new Map<String, String>();
		this.params = new Map<String, String>();
	}

	public HttpCalloutService(String name_of_setting, Boolean isWebservice) {
		this();
		WebserviceSettingSelector selector = new WebserviceSettingSelector();
		WebserviceSettings__mdt webservice_selected = selector.selectOneWithHeadersByDeveloperName(name_of_setting);

		this.endpoint = webservice_selected.WebserviceURL__c;
		for(HeaderVariables__mdt header_variable : webservice_selected.Header_variables__r) {
			this.headers.put(header_variable.MasterLabel, header_variable.Value__c);
		}

	}
	
	/***
	* Constructor of class HttpCalloutService.
	* @param  endpoint to the external web service.
	*/
	public HttpCalloutService(String endpoint) {
		this();
		this.endpoint = endpoint;
	}

	/***
	* Metgod to add url paramter.
	* @param  key   Url parameter name.
	* @param  value Value of the given Url parameter.
	*/
	public HttpCalloutService addUrlParam(String key, String value) {
		this.params.put(key, value);
        return this;
	}

	/***
	* Metgod to add header line.
	* @param  key   Header line parameter name.
	* @param  value Value of the given header line parameter.
	*/
	public HttpCalloutService addHeaderLine(String key, String value) {
		this.headers.put(key, value);
        return this;
	}

	/***
	* @description Tells the HTTP library that it's going to talk to it's own Salesforce instance
	* endpoint address then becomes the URI resource ( not the actual endpoint ) and inject authorization headers
	* works for all apex except for lightning and if invoked by an automated process ( which is not runned as a user )
	* @returns HttpCalloutService object
	* @example
	* HttpResponse response = new HttpCalloutService('/services/data/v44.0/limits').callOwnSalesforce().get();
	* System.debug(response.getBody());
	*/
	public HttpCalloutService callOwnSalesforce() {
		this.callingOwnSalesforce = true;
		return this;
	}

	public HttpCalloutService handleOwnException() {
		this.handleOwnException = true;
		return this;
	}

	/***
	* @description Tells the library to convert all parameters to Query Parameters
	* @returns HttpCalloutService object
	* @example
	* HttpResponse response = new HttpCalloutService('/services/data/v44.0/limits').callOwnSalesforce().convertParamstoQueryString().get();
	* System.debug(response.getBody());
	*/
	public HttpCalloutService convertParamstoQueryString() {
		this.convertParamstoQueryString = true;
		return this;
	}
	
    public HttpCalloutService setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public HttpCalloutService setHeaders(Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

	/***
	* Send a get request to the instainstantiated HttpCalloutService object.
	* @return HTTPResponse in case of success or null on failure.
	* @throws HTTPResultCodeException if the Code is not valid
	* TODO: Improve the HTTPResultCodeException for more information
	*/
	public HttpRequest buildRequest(String method, String body) {
		HttpRequest req = new HttpRequest();
		if(this.contentType == null && method != 'GET')
            this.contentType = 'application/json';

		if (this.contentType != null && this.contentType.length() > 0) {
			req.setHeader('Accept', this.contentType);
			req.setHeader('Content-Type', this.contentType);
		}

        if (this.headers != null && this.headers.keySet().size()>0) {
            for (String key : this.headers.keySet()) {
            	String value = this.headers.get(key);
            	value = value == null ? '' : value;
                req.setHeader(key, value);
            }
        }

        if (body != null && body.length() > 0) {
            req.setBody(body);
        }

        if((method == 'GET' && this.params.size() > 0) || convertParamstoQueryString) {
            paramsToQueryString();
        }

        req.setMethod(method);
        req.setTimeout(TIMEOUT);

		// If callingOwnSalesforce we expect the endpoint to be the REST Api Endpoint
		if(this.callingOwnSalesforce) {
			this.endpoint = System.Url.getOrgDomainUrl().toExternalForm() + this.endpoint;
			req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
		}
		
        req.setEndpoint(this.endpoint);
        log('Request', req, req.getBody());
        return req;
	}

    /*******************************************************************************************************
    * @description Sends the any method down to a HTTP server. 
    * @param method Type of method sent down to the server
	* @param body Body of the request being sent down
    * @return HttpResponse object
    * @example
    * HttpCalloutService calloutService = new HttpCalloutService();
	* HttpResponse response = callout.sendRequest('GET', null);
    */
    public HttpResponse sendRequest(String method, String body) {
        Http http = new Http();
        HttpRequest req = buildRequest(method, body);

    	HTTPResponse res = http.send(req);

        if((res.getStatusCode() >= 200 && res.getStatusCode() <= 300) || this.handleOwnException) {
            log('Response', res, res.getBody());
            return res;
        }
        else {
            throw new HTTPResultCodeException('Failed to recieve a success code from remote. Code was: ' + res.getStatusCode() + ' request was ' + req + ' Response Body is: ' + res.getBody());
        }
    }

    /*******************************************************************************************************
    * @description Sends the any method down to a HTTP server. 
    * @param method Type of method sent down to the server
    * @return HttpResponse object
    * @example
    * HttpCalloutService calloutService = new HttpCalloutService();
	* HttpResponse response = callout.sendRequest('GET');
    */
	public HttpResponse sendRequest(String method) {
		return this.sendRequest(method, null);
	}

	/***
	* @deprecated Should not be used and should be removed in favour of get
	* Send a get request to the instainstantiated HttpCalloutService object.
	* @return HTTPResponse in case of success or null on failure.
	*/
	public HTTPResponse sendGetRequest() {
		return sendRequest('GET');
	}

    /*******************************************************************************************************
    * @description Sends the get method down to a HTTP server. Retrieves information. GET Request
	* must be safe and idempotent. Sends it with a parameter map.
    * @param paramsMap A map of String, String object that contains parameters to be used
	* will become queryString parameters in the GET request
    * @return HttpResponse object
    * @example
    * HttpCalloutService calloutService = new HttpCalloutService();
	* Map<String, String> paramMap = new Map<String, String>(); 
	* paramMap.put('ParamTest', 'test');
	* HttpResponse response = callout.get(paramMap);
    */
	public HttpResponse get(Map<String, String> paramsMap) {
		if(paramsMap != null) { 
			this.params = paramsMap;
		}

		return sendRequest('GET');
	}

    /*******************************************************************************************************
    * @description Sends the get method down to a HTTP server. Retrieves information. GET Request
	* must be safe and idempotent. 
    * @return HttpResponse object
    * @example
    * HttpCalloutService calloutService = new HttpCalloutService();
	* HttpResponse response = callout.get();
    */
    public HttpResponse get() {
        return this.get(null);
    }


    /*******************************************************************************************************
    * @description Sends the put method down to a HTTP server. Request that the resource at the URI
	* does something with the provided entity. Often used to update or create a entity. Idempotency
	* is often the main difference between PUT vs POST
    * @param body The body that shall be sent to the HTTP Server
    * @return HttpResponse object
    * @example
    * HttpCalloutService calloutService = new HttpCalloutService();
	* HttpResponse response = callout.put(null);
    */
	public HttpResponse put(String body) {
        return sendRequest('PUT', body);
	}

    /*******************************************************************************************************
    * @description Sends the post method down to a HTTP server. Request that the resource at the URI
	* does something with the provided entity. Often used to create new entities.
    * @param body The body that shall be sent to the HTTP Server
    * @return HttpResponse object
    * @example
    * HttpCalloutService calloutService = new HttpCalloutService();
	* HttpResponse response = callout.post(null);
    */
    public HttpResponse post(String body) {
        return sendRequest('POST', body);
    }

    /*******************************************************************************************************
    * @description Sends the patch method down to a HTTP server. Update only specified field of an entity
	* uses X-HTTP-Method-Override since PATCH is not supported by Salesforce on Apex outbound
    * @param body The body that shall be sent to the HTTP Server
    * @return HttpResponse object
    * @example
    * HttpCalloutService calloutService = new HttpCalloutService();
	* HttpResponse response = callout.patch(null);
    */
	public HttpResponse patch(String body) {
		this.headers.put('X-HTTP-Method-Override', 'PATCH');
        return sendRequest('POST', body);
	}

    /*******************************************************************************************************
    * @description Sends the delete method down to a HTTP server. Typically used to delete a resource 
	* on the server. It's called sendDelete since delete is reserved token inside of Salesforce and
	* we can't use that
    * @param body The body that shall be sent to the HTTP Server
    * @return HttpResponse object
    * @example
    * HttpCalloutService calloutService = new HttpCalloutService();
	* HttpResponse response = callout.sendDelete(null);
    */
	public HttpResponse sendDelete(String body) {
        return sendRequest('DELETE', body);
	}

    private void paramsToQueryString() {
    	if(this.params == null || this.params.size() <= 0)
            return;
        String paramsList = '';
    	Boolean isFirst = true;

        for(String key : this.params.keySet()) {
        	paramsList += isFirst ? '?' : '&';
        	isFirst = false;

        	String param = this.params.get(key) == null ? '' : this.params.get(key);

        	paramsList += key + '=' + EncodingUtil.urlEncode(param,'UTF-8');
        }


        this.endpoint += paramsList;
    }

	public void log(String header, Object obj, String msg) {
		String startStop = '\n========================CALLOUT========================';
		String logOutput = startStop;
		logOutput += (header != null) ? '\n== Header: ' + header : 'Header: No Header Set';
		logOutput += (obj != null) ? '\n== Obj string Rep: ' + obj : '\n No Obj set';
		logOutput += (msg != null) ? '\n== ' + msg : '';
		logOutput += startStop;
		if(DEBUG){
			lastDebugMessage = logOutput;
			System.debug(logOutput);
		}

	}
}