trigger COM_Account on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    fflib_SObjectDomain.triggerHandler(COM_AccountTrigger.class);
}