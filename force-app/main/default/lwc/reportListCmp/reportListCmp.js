import {LightningElement, api, track, wire} from 'lwc';
import selectReportsByDeveloperName from '@salesforce/apex/COM_ReportListCmpController.selectReportsByFolderDeveloperName';
import reportListLabel from '@salesforce/label/c.reportListLabel';

export default class ReportListCmp extends LightningElement {

    @api folderName;
    @track reports;
    @track error;

    @wire(selectReportsByDeveloperName, {folderName: '$folderName'})
    wiredReports({error, data}){
        if(data){
            this.reports = data;
        } else if (error){
            this.error = error;
        }
    }

    label = {
        reportListLabel
    }
}